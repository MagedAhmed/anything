<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContactsController extends Controller
{
	public function index()
	{
		$counter = 1;
		$contacts = \App\Contact::all();
		return view('cpanel.layout.contacts.index',compact('contacts','counter'));
	}
    public function store(Request $request)
    {
    	\App\Contact::create($request->all());
    	return redirect('/');
    }
}
