<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Offer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Image;
use Input;
class OffersController extends Controller
{
    public function index()
    {
    	$offers = Offer::all();
    	$counter = 1;
        return view('cpanel.layout.offers.index',compact('offers','counter'));
    }

    public function create()
    {
    	return view('cpanel.layout.offers.create');
    }

    public function store(Request $request)
    {	

    	if(count(\App\Offer::all()) >= 4 ){
    		Session::flash('error','لا يمكنك عمل اكثر من 4 عروض');
    		return back();
    	}

        //
        $offer = new \App\Offer;
        $offer->name = $request->name;
        if($request->hasFile('photo'))
        {
  
            $image = Input::file('photo');
            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('/upload/offers/' . $filename);
 
        
            Image::make($image->getRealPath())->resize(500, 350)->save($path);
                $offer->photo = $filename;
                $offer->save();
           }
        //


        
    	Session::flash('success','تم اضافة العرض');
        return back();
    }






    public function edit($id)
    {
    	$offer = Offer::find($id);
    	return view('cpanel.layout.offers.edit',compact('offer')); 
    }

    public function update(Request $request,$id)
    {
    	
    	$offer = Offer::find($id);
    	if($request->file('photo'))
    	{
    		unlink(public_path('/upload/offers/'.$offer->photo));
    		$photo = $request->file('photo');
            $path = public_path().'/upload/offers';
            $extension = $photo->getClientOriginalExtension();
            $name = time().'.'.$extension;
            if($photo->move($path,$name)){
                $offer->photo = $name;
            }
    	}
    	$offer->name = $request->name;
    	$offer->save();
    	Session::flash('success','تم تعديل العرض');
        return back();
    }
}
