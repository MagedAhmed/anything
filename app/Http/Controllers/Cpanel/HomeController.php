<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
    	$offers = count(\App\Offer::all());
    	$contacts = count(\App\Contact::all());
    	
        return view('cpanel.layout.home',compact('offers','contacts'));
    }
}
