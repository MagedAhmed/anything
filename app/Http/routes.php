<?php

view()->share('cpanel',url('public/cpanel').'/');

view()->share('theme',url('public/theme').'/');

view()->share('offerImage',url('public/upload/offers').'/');


define('cp','cpanel');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix'=> cp,'middleware' => 'auth'],function(){
	Route::get('/contacts','ContactsController@index');

	Route::get('/','Cpanel\HomeController@index');
	Route::resource('/offers','Cpanel\OffersController');
});


Route::post('/contacts','ContactsController@store');

Route::get('/', function () {
	$offers = \App\Offer::all();
    return view('welcome',compact('offers'));
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

Route::auth();

Route::get('/home', 'HomeController@index');
