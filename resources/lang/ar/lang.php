<?php 


return [

	'site_name' => 'اسم الموقع',
	'name' => 'اﻻسم',
	'email' => 'البريد اﻻلكتروني',
	'password' => 'كلمة المرور',
	'password_confirmation' => 'تأكيد كلمة المرور',
	'remember' => 'تذكرني',
	'login' => 'تسجيل الدخول',
	'register' => 'إنشاء حساب',
	'forget_password' => 'نسيت كلمة المرور',
	'sign_out' => 'تسجيل الخروج',
	'main' => 'الرئيسية',
	'contact_us' => 'الرسائل',
	'offers' => 'العروض',


];