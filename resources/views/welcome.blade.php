@extends("theme.layout")

@section("header")
    <!--BOOTSTRAP CSS STYLE-->
    <link href="{{ $theme }}bootstrap/css/bootstrap.min.rtl.css" rel="stylesheet" type="text/css">

    <!--Font Awesome css-->
    <link href="{{ $theme }}font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--OWL SLIDER CSS STYLE-->
    <link href="{{ $theme }}owl/css/owl.carousel.min.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}owl/css/owl.theme.default.min.rtl.css" rel="stylesheet" type="text/css">

    <!--CUSTOME CSS-->
    <link href="{{ $theme }}css/main.rtl.css" rel="stylesheet" type="text/css">

    <link href="{{ $theme }}css/index.rtl.css" rel="stylesheet" type="text/css">

    <script src="{{ $theme }}js/modernizr.min.js"></script>
@stop



@section("content")

<div id="slides" class="section banner">
    <ul class="slides-container">
        <li>
            <img src="{{ $theme }}/images/slide-1.jpg" alt="">
            <div class="carousel-caption">
                <div class="container">
                    <div class="wrap-caption">
                        <div class="caption-heading">
                            <div class="color1">
                                <span>لدينا خبره كبيره</span>
                            </div>
                            <div class="color2">
                                <span>فى مكافحه الحشرات</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </li>
        <li>
            <img src="{{ $theme }}/images/slide-2.jpg" alt="">
            <div class="carousel-caption">
                <div class="container">
                    <div class="wrap-caption">
                        <div class="caption-heading">
                            <div class="color1">
                                <span>قم بحمايه </span>
                            </div>
                            <div class="color2">
                                <span>منزلك اليوم</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <img src="{{ $theme }}/images/slide-3.jpg" alt="">
            <div class="carousel-caption">
                <div class="container">
                    <div class="wrap-caption">
                        <div class="caption-heading">
                            <div class="color1">
                                <span>نحن نهتم ب </span>
                            </div>
                            <div class="color2">
                                <span>القضاء على الحشرات</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

    </ul>
</div>

<!-- PEST CONTROL -->
<div class="section pest-control bgc">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="section-title">
                    <h3 class="lead">خدماتنا</h3>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 text-xs-right">
                <a href="{{url('/services')}}" title="" class="btn btn-default btn-view-all">شاهد المزيد</a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/tanzeef.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            الشركة بها العمالة المدربة والأجهزة المناسبة لتنظيف المباني والفيلات من الداخل والخارج والحصول علي افضل النتائج في أسرع وقت ممكن
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">تنظيف المباني والفيلات</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/pest-1.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            حشرة من رتبة مستقيمة الأجنحة صغير له ست أرجل، أجنحة شفافة بنية اللون، جسمه مفلطح بيضي يضرب لونه للسمرة، و له قرنا الاستشعار طويلان والعينان كبيرتان وللصرصور رائحة كريهة من إفراز غدي
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">القضاء على الصراصير</a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/segad.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            يعدّ السجاد من القطع المهمة في المنزل، ولا يمكن اعتبارها من كماليات المنزل، بل هي ضرورية لجعل المنزل أجمل، وإظهار الأثاث بصورة أكثر أناقة، كما تساعد على دفء المنزل في فصل الشتاء، ولكن غسل السجاد يتطلب الكثير من الوقت والجهد

                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">تنظيف السجاد</a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/ba2.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
بق الفراش حشرة صغيره (غير مجنحة وطولها (4 - 7 مليمتر) ولونها يميل إلى البني الداكن وبيضاوية الشكل) تمص الدماء أثناء الليل وتسبب الحكة الشديدة والحساسية والإرهاق بسبب عدم النوم، تتغذى على الدم خصوصا أثناء نوم الإنسان ليلا أو نهاراً في الغرف المظلمة
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">مكافحه البق</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/t3abeen.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            الثعبان أو الأفعى أو الحية هو حيوان زاحف من ذوات الدم البارد يتبع رتيبة Serpentes من رتبةالحرشفيات له جسم متطاول، مغطى بحراشف، ولا توجد له أطراف، أو أذنين خارجيتين، وجفون ولكن ثمة حواف في جسمه، يعتقد أنها كانت تمثل أطرافه التي تلاشت
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">مكافحه الثعابين والأفاعي</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/elrama.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            النمل الأبيض حشرة كانسة كما يتسم بحياته السرية, ويتغذى أساسا على السيليولوز, ويعيش في مستعمرات تتنوع فيها الطوائف التي تختلف في بنيتها ووظيفة كل منها
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">مكافحه الرمة</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/pest-6.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                            الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                            تؤثر على صحة الإنسان والصحه العامه.
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">مكافحه الحشرات الطائره</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/pest-7.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            
يعد النمل من الحشرات التى تؤثر سلبيا على حياة الانسان حيث يتميز بسرعة التكاثر والانتشار بشكل كبير جدا ليغزو المنازل والشركات والمطاعم والمخازن للبحث عن المأوى والغذاء مما يؤدى الى فساد العديد من المواد الغذائية والتى تحتوى على السكريات مما يؤدى الى تلوث المواد الغذائية وانتشار البكتريا والجراثيم عليها مما يشكل خطوة كبرى على الانسان 

                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">القضاء على النمل</a>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/pest-8.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            تعد الفئران من اكثر القوارض انتشارا, حيث تتواجد فى غالبية اسطح المبانى والشركات والمخازن والمطاعم والمزارع, وتتسبب الفئران فى العديد من المشاكل إن تواجدت فى منزلك حيث تسبب العديد من الاضرار بالملابس والاسلاك والاساس المنزلي, ايضا يوجد العديد من الامراض التى قد تتسبب بها.
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">مكافحه الفئران</a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/na7l.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            لسعة النحل هي طريقة دفاعية تقوم بها نحلة أو مجموعة من النحل للدفاع عن نفسها أو عن عشها من الآفات الشرسة كالحشرات أو الحيوانات التي تأكلها أو تأكل عسلها أو حتى الإنسان تقوم بالهجوم عليه لأسباب منها أخذ العسل من عشها[1] النحل يموت عندما يلسع اي كائن و ذلك يرجع إلى انفصال جزء من امعاء النحلة.
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">التخلص من النحل</a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/scorpion.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                            يعيش في المناطق الحارة والجافّة، مختبئا في الجحور والشقوق ،وتحت الحجارة والصخور، التماسا للرطوبة ،وتجنبا لحرارة الشمس.يتواجد من العقارب في العالم حوالي 2000 نوع.معظم العقارب سامة إذ تتواجد الغدة السامة في نهاية الذيل.
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">التخلص من العقارب</a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img src="{{ $theme }}/images/moket.jpg" alt="" class="img-responsive"></a>
                    </div>
                    <div class="data">
                        <p>
                             في بعض المنازل يتم استخدام الموكيت كحل عملي في المساحات الكبيرة و غرف اﻷطفال ، ولكن تأتي مهمة تنظيفه بشكل دوري أمرًا غاية في اﻷهمية حتي لا يتسبب تراكم اﻷتربة في التأثير على صحة اﻷسرة .
                        </p>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">تنظيف الموكيت</a>
                        </h5>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- STATS -->
<div class="section stats bg1">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-users"></div>
                    </div>
                    <div class="text-xs-left">
                        <h2 class="counter text-xs-left">20+</h2>
                        <p>موظف</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-building-o"></div>
                    </div>
                    <h2 class="counter">50+</h2>
                    <p>عامل</p>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-heart-o"></div>
                    </div>
                    <h2 class="counter">2000+</h2>
                    <p>متابع</p>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-trophy"></div>
                    </div>
                    <h2 class="counter">24+</h2>
                    <p>عميل</p>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="section-title">
                    <h3 class="lead">أخر عروضنا</h3>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($offers as $offer)
            <div class="col-sm-4 col-md-3">
                <div class="box-image">
                    <div class="image">
                        <a href="{{url('/services')}}" title=""><img   src="{{$offerImage}}/{{$offer->photo}}" alt="العرض" class="img-responsive"></a>
                    </div>
                    <div class="description">
                        <h5 class="blok-title">
                            <a href="{{url('/services')}}" title="">{{$offer->name}}</a>
                        </h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<!-- WHY -->
<div class="section why">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="section-title">
                            <h3 class="lead">ليه تختارنا؟</h3>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <div class="box-icon">
                            <div class="icon">
                                <div class="fa fa-clock-o"></div>
                            </div>
                            <div class="box-icon-body text-xs-left">
                                <h5>التنظيف و الكشف السريع للحشرات</h5>
                                <p>بإستخدام احدث اﻻساليب الآمنة تماماً للتنظيف والقضاء علي الحشرات
                                </p></div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="box-icon">
                            <div class="icon">
                                <div class="fa fa-table"></div>
                            </div>
                            <div class="box-icon-body text-xs-left">
                                <h5>متابعه شهريه</h5>
                                <p>نضمن لعملائنا كافة برامجنا وحتى سنوات من تنفيذها, فنحن نملك الخبرة لتقديم ضمان على كافة خدماتنا لسنوات
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="box-icon">
                            <div class="icon">
                                <div class="fa fa-briefcase"></div>
                            </div>
                            <div class="box-icon-body text-xs-left">
                                <h5>ازاله تامه للحشرات</h5>
                                <p>نقضي على الحشرات نهائيا بخبرة لأكثر من 10 سنوات فى مجال مكافحه الحشرات وبأحدث الطرق العلميه والاساليب التكنولوجية
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="box-icon">
                            <div class="icon">
                                <div class="fa fa-money"></div>
                            </div>
                            <div class="box-icon-body text-xs-left">
                                <h5>اسعار مقبوله</h5>
                                <p>تقدم الشركة عروض بشكل مستمر لتصل خدماتنا للجميع بأفضل الأسعار.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-4 col-md-4">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="section-title">
                            <h3 class="lead">من نحن؟</h3>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="box-image-2">
                            <div class="image animg">
                                <!--<img src="{{ $theme }}/images/we-1.jpg" alt="" class="img-responsive">-->
                            </div>
                            <div class="box-image-body">
                                <p>

تعتبر شركة صلاح الدين لتنظيف المباني و ابادة ومكافحة الحشرات من اوائل الشركات التى تعتمد على استخدام التقنيات الحديثة فى عمليات التنظيف للمباني والفيلات والسجاد ومكافحة الحشرات
 ، و تتميز الشركه بأن لدينا افضل العمالة المدربة ومهندسين متخصصون فى مجال تنظيف المباني و مكافحة الحشرات لذلك تقوم الشركة بمتابعه دائمة ومستمرة للوصول لأفضل النتائج .

                                </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<!-- TESTIMONY -->
<!-- <div class="section testimony bgi-2">
    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="section-title center cwhite">
                    <h3 class="lead">الاراء</h3>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12 col-md-12">
                <div id="owl-testimony">
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-testimony">
                            <div class="quote-box">
                                <blockquote class="quote">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.


                                </blockquote>
                            </div>
                            <div class="people">
                                <img class="img-rounded user-pic" src="{{ $theme }}/images/testimony-thumb-1.jpg" alt="">
                                <p class="details">
                                    محمد ياسين <span>رئيس شركه</span>
                                </p>
                            </div>
                            <div class="icon"><span class="fa fa-quote-left"></span></div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div> -->

@stop


@section("footer")
<!--JAVA SCRIPT-->

<!--JQUERY SCROPT-->
<script src="{{ $theme }}js/jquery.min.js"></script>

<!--BOOTSTRAP SCRIPT-->
<script src="{{ $theme }}bootstrap/js/bootstrap.min.js"></script>

<!--SLIDER SCRIPT-->
<script src="{{ $theme }}owl/js/owl.carousel.js"></script>
<script src="{{ $theme }}owl/js/jquery.superslides.js"></script>

<!--CUSTOM JS-->
<script src="{{ $theme }}js/main.js"></script>
@stop

