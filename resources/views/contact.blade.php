@extends("theme.layout")

@section("header")
    <!--BOOTSTRAP CSS STYLE-->
    <link href="{{ $theme }}bootstrap/css/bootstrap.min.rtl.css" rel="stylesheet" type="text/css">

    <!--Font Awesome css-->
    <link href="{{ $theme }}font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--OWL SLIDER CSS STYLE-->
    <link href="{{ $theme }}owl/css/owl.carousel.min.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}owl/css/owl.theme.default.min.rtl.css" rel="stylesheet" type="text/css">

    <!--CUSTOME CSS-->
    <link href="{{ $theme }}css/main.rtl.css" rel="stylesheet" type="text/css">

    <link href="{{ $theme }}css/index.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}css/services.rtl.css" rel="stylesheet" type="text/css">

    <script src="{{ $theme }}js/modernizr.min.js"></script>
@stop

@section("content")
<!-- BANNER -->
<div class="section subbanner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="caption">تواصل معنا</div>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">الرئيسيه</a></li>
                    <li class="active">الاتصال</li>
                </ol>

            </div>
        </div>
    </div>
</div>

<!-- CONTACT  -->
<div class="section contact">
    <div class="container">
        <div class="row">

            <div class="col-sm-6 col-md-6">

                <div class="section-title">
                    <h3 class="lead">تواصل مع صلاح الدين</h3>
                </div>
                <p class="">
                    يمكنك التواصل معنا في أي وقت
                </p>                   <div class="contact-info">
                    <div class="contact-info-item">
                        <span class="fa fa-phone"></span>  050 518 559 
                    </div>
                    <div class="contact-info-item">
                        <span class="fa fa-clock-o"></span> العمل طوال أيام اﻻسبوع
                    </div>
                    <div class="contact-info-item">
                        <span class="fa fa-envelope"></span> <a href="mailto:" >info@salahdien.com</a>
                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="post-image">
                    <img src="{{ $theme }}images/blog-4.jpg" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <div class="maps-wraper">
                    <div id="cd-zoom-in"></div>
                    <div id="cd-zoom-out"></div>
                    <div id="maps" class="maps" data-lat="-7.452278" data-lng="112.708992" data-marker="images/cd-icon-location.png">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-md-offset-2">

                <br /><br />
                <div class="section-title center">
                    <h3 class="lead">ارسل رسالتك</h3>
                </div>
                <br /><br />
                <div class="blok quotes">
                    
                    {!! Form::open(['url' => '/contacts', 'id' => 'contactForm','class' => 'form-contact shake','novalidate' => 'true','data-toggle' => 'validator']) !!}

                    
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="الاسم الاول *" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" id="email" placeholder="ادخل رقم الهاتف *" required="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea id="message" name="message" class="form-control" rows="4" placeholder="ادخل رسالتك *" required=""></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-default disabled" style="pointer-events: all; cursor: pointer;">اسأل</button>
                        </div>
                    {!! Form::close() !!}
                    <div class="icon"><span class="fa fa-calendar"></span></div>
                </div>

            </div>

        </div>
    </div>



</div>


@stop


@section("footer")
<!--JAVA SCRIPT-->

<!--JQUERY SCROPT-->
<script src="{{ $theme }}js/jquery.min.js"></script>

<!--BOOTSTRAP SCRIPT-->
<script src="{{ $theme }}bootstrap/js/bootstrap.min.js"></script>

<!--SLIDER SCRIPT-->
<script src="{{ $theme }}owl/js/owl.carousel.js"></script>
<script src="{{ $theme }}owl/js/jquery.superslides.js"></script>

<!--CUSTOM JS-->
<script src="{{ $theme }}js/main.js"></script>
@stop
