@if(Session::has('success'))
	<div class="alert alert-success">
		<h1 class="text-center">{{session()->get('success')}}</h1>
	</div>
@endif

@if(Session::has('error'))
	<div class="alert alert-danger">
		<h1 class="text-center">{{session()->get('error')}}</h1>
	</div>
@endif