@extends('cpanel.layout.index')

@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        عرض جديد
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/cpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
            <!-- form start -->
            {!! Form::open(['url' => 'cpanel/offers', 'id' => 'review-form','class' => 'smart-form','files'=> true]) !!}
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputPassword1">العرض</label>
                  <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="ادخل العرض" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input name="photo" type="file" id="exampleInputFile" required>

                  <p class="help-block">صورة تعبر عن العرض</p>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">نشر</button>
              </div>
            {!! Form::close() !!}
          </div>
     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
