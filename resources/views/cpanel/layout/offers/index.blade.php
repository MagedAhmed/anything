@extends('cpanel.layout.index')
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{trans('lang.offers')}}
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/cpanel')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <p>...</p>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          		<div class="box">
            <div class="box-header with-border">
              <h3 style="margin-top:10px;" class="box-title">قائمة العروض</h3>
              <a href="{{url('/cpanel/offers/create')}}" style="float:left" class="btn btn-lg btn-success">عرض جديد</a>
            </div>
            <!-- /.box-header -->
            @if(isset($offers))
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 20px">#</th>
                  <th style="width:80%; text-align:right;">العرض</th>
                  <th style="text-align:right">العملية</th>
                </tr>
                @foreach($offers as $offer)
                <tr>
                  <td>{{ $counter++ }}</td>
                  <td>{{ $offer->name }}</td>
                  <td>
                    <a href="{{url('/cpanel/offers/'.$offer->id.'/edit')}}" class="btn btn-info">تعديل</a>
                    <a href="" class="btn btn-danger">حذف</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            @endif
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul  class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">{{$counter-1}}</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
