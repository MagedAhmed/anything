@extends('cpanel.layout.index')

@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        عرض جديد
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/cpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
            <!-- form start -->
            {!! Form::open(['url' => 'cpanel/offers/'.$offer->id , 'method' => 'put', 'id' => 'review-form','class' => 'smart-form','files' =>true]) !!}
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputPassword1">العرض</label>
                  <input value="{{$offer->name}}" type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="ادخل العرض" required>
                </div>
                <div class="form-group">
                  <img style="width:500px; height:350px;" class="img-responsive" src="{{$offerImage}}/{{$offer->photo}}" />
                  <label for="exampleInputFile">تعديل الصورة ؟</label>
                  <input value="{{$offer->photo}}" name="photo" type="file" id="exampleInputFile">

                  <p class="help-block">صورة تعبر عن العرض</p>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">تثبيت التعديل</button>
                <a href="{{url('/cpanel/offers')}}" class="btn btn-warning">العودة</a>
              </div>
            {!! Form::close() !!}
          </div>
     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
