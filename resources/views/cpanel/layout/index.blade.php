@include('cpanel.layout.inc.head')
@include('cpanel.layout.inc.header')

@include('cpanel.layout.inc.sidebar')
@include('cpanel.notifications')

@yield('content')

@include('cpanel.layout.inc.footer')
