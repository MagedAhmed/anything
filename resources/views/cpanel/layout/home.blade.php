@extends('cpanel.layout.index')

@section('content')

<section class="content">
	<div class="row">
        <div class="col-lg-5 col-xs-10">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="text-center">
              <h3>{{$contacts}}</h3>

              <p>عدد الرسائل</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{url('/cpanel/contacts')}}" class="small-box-footer">لمعرفة المزيد <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-5 col-xs-10">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="text-center">
              <h3>{{$offers}}</h3>

              <p>عدد العروض الحالية</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url('/cpanel/offers')}}" class="small-box-footer">لمعرفة المزيد<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        
      </div>
</section>
@stop