@extends('cpanel.layout.index')
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{trans('lang.contact_us')}}
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/cpanel')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <p>...</p>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          		<div class="box">
            <div class="box-header with-border">
              <h3 style="margin-top:10px;" class="box-title">الرسائل</h3>
            </div>
            <!-- /.box-header -->
            @if(isset($contacts))
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 20px">#</th>
                  <th style="text-align:right;">الاسم</th>
                  <th style="text-align:right;">رقم الهاتف</th>
                  <th style="width:50%; text-align:right">الرسالة</th>
                </tr>
                @foreach($contacts as $contact)
                <tr>
                  <td>{{ $counter++ }}</td>
                  <td>{{ $contact->name }}</td>
                  <td>{{ $contact->phone }}</td>
                  <td>{{ $contact->message }}</td>
                </tr>
                @endforeach
              </table>
            </div>
            @endif
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul  class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
