<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>صلاح الدين</title>

    <!--META TAGS-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="icon" type="image/png" href="{{ $theme }}/images/footer-logo.png">

    @yield("header")
  
</head>
<body data-spy="scroll" data-target="#main-menu">

<!--<header>-->
<div class="navbar navbar-main navbar-fixed-top" id="main-menu">
    <div class="container">
        <div class="row bg-white">
            <div class="topbar">
                <div class="col-sm-7 col-md-7">
                    <div class="info">
                        <div class="info-item">
                            <span class="fa fa-phone"></span> 050 518 559
                        </div>
                        <div class="info-item">
                            <span class="fa fa-clock-o"></span> 
                            العمل طوال أيام الإسبوع
                        </div>
                        <div class="info-item">
                            <span class="fa fa-envelope-o"></span> <a href="mailto:" title="">info@salaheldein.com</a>
                        </div>

                    </div>
                </div>
                <div class="col-sm-5 col-md-5 text-xs-right">
                    <p class="info ">
                        دبي - عجمان - الشارقه - ام القوين
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container container-nav">
        <div class="navbar-header">
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse"
                    data-target="#exCollapsingNavbar2" aria-controls="exCollapsingNavbar2" aria-expanded="false"
                    aria-label="Toggle navigation">
                &#9776;
            </button>
            <a class="navbar-brand hidden-md-down" href="{{url('/')}}">صلاح الدين</a>
        </div>

        <nav class="collapse navbar-toggleable-xs pull-xs-right" id="exCollapsingNavbar2">
            <ul class="nav navbar-nav">
                <li class="nav-item @if (url()->current() == url('/')) active @endif">
                    <a class="nav-link" href="{{url('/')}}">الرئيسيه <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item @if (url()->current() == url('/services')) active @endif">
                    <a class="nav-link" href="{{ url('/services') }}">خدماتنا</a>
                </li>
                <li class="nav-item @if (url()->current() == url('/contact')) active @endif">
                    <a class="nav-link" href="{{ url('/contact') }}">اتصل بنا</a>
                </li>
                <li class="nav-item @if (url()->current() == url('/about')) active @endif">
                    <a class="nav-link" href="{{url('/about')}}">عن صلاح الدين</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!--</header>-->

@yield("content")




<!-- CTA -->
<div class="section cta">
    <div class="container">

        <div class="row">

            <div class="col-sm-12 col-md-12">
                <div class="wrap-cta">
                    <div class="cta-img ">
                        <img src="{{ $theme }}/images/img-cta.png" alt=""/>
                    </div>
                    <div class="cta-desc pull-xs-left text-xs-left">
                        <p>لديك أى استفسار </p>
                        <h3>لا تتردد فى الاتصال بنا</h3>
                    </div>
                    <a href="{{url('/contact')}}" title="" class="btn btn-contact pull-xs-right btn-view-all">اتصل بنا</a>

                </div>
            </div>

        </div>
    </div>
</div>

<!-- FOOTER SECTION -->
<div class="footer bgi-footer">

    <div class="container">

        <div class="row">
            <div class="col-sm-4 col-md-4">
                <div class="box-info text-xs-left">
                    <div class="box-info-body">
                        <p>لديك اى سؤال اتصل بنا الان</p>
                        <h4>+050 595 565</h4>
                    </div>
                    <div class="box-info-icon">
                        <span class="fa fa-phone"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="box-info text-xs-left">
                    <div class="box-info-body">
                        <p>متواجدين دائما</p>
                        <h4>العمل طوال أيام الإسبوع</h4>
                    </div>
                    <div class="box-info-icon">
                        <span class="fa fa-clock-o"></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="box-info text-xs-left">

                    <div class="box-info-body">
                        <p>تواصل معنا عن ظريق الميل</p>
                        <h4><a href="mailto:" title="">Support@salah.com</a></h4>
                    </div>
                    <div class="box-info-icon">
                        <span class="fa fa-phone"></span>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- <div class="fsosmed">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="footer-sosmed">
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </a>
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </a>
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-pinterest"></i>
                            </div>
                        </a>
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-google"></i>
                            </div>
                        </a>
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-instagram"></i>
                            </div>
                        </a>
                        <a href="{{ url('/') }}" title="">
                            <div class="item">
                                <i class="fa fa-linkedin"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="fcopy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p class="ftex">&copy; 2016 Salah Eldien by Majed Ahmed & Dina Reda - All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>

</div>


@yield("footer")

</body>
</html>
