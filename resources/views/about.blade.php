@extends("theme.layout")
@section("header")
    <!--BOOTSTRAP CSS STYLE-->
    <link href="{{ $theme }}bootstrap/css/bootstrap.min.rtl.css" rel="stylesheet" type="text/css">

    <!--Font Awesome css-->
    <link href="{{ $theme }}font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--OWL SLIDER CSS STYLE-->
    <link href="{{ $theme }}owl/css/owl.carousel.min.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}owl/css/owl.theme.default.min.rtl.css" rel="stylesheet" type="text/css">

    <!--CUSTOME CSS-->
    <link href="{{ $theme }}css/main.rtl.css" rel="stylesheet" type="text/css">

    <link href="{{ $theme }}css/index.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}css/services.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}css/about.rtl.css" rel="stylesheet" type="text/css">

    <script src="{{ $theme }}js/modernizr.min.js"></script>


@stop


@section("content")
<!-- BANNER -->
<div class="section subbanner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="caption">عن صلاح الدين</div>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">الرئيسيه</a></li>
                    <li class="active">عننا</li>
                </ol>

            </div>
        </div>
    </div>
</div>

<!-- WHY  -->
<div class="section why">
    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="section-title">
                            <h3 class="lead">ليه تختارنا</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="box">
                            <img src="{{ $theme }}images/why-img.png" alt="" class="img-fluid" />
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <p class="">
                                    لأننا نستخدم أفضل الوسائل في تنظيف المباني والسجاد والموكيت والقضاء علي الحشرات والثعابين والعقارب باستخدام احدث الأسايب والمبيدات الحشرية المستخدمة عالميا فى عمليات الرش في اماكن تواجد
                                    الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                                    تؤثر على صحة الإنسان والصحه العامه.
                                </p>
                                <br />
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="box-icon">
                                    <div class="icon">
                                        <div class="fa fa-clock-o"></div>
                                    </div>
                                    <div class="box-icon-body">
                                        <h5>فحص من اول مره</h5>
                                        <p class="">
                                            الكشف السريع عن الحشرات وبيوتها وتقدير حجم المشكلة ووضع خطة لإزالتها والتخلص منها.
                                        </p>                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="box-icon">
                                    <div class="icon">
                                        <div class="fa fa-table"></div>
                                    </div>
                                    <div class="box-icon-body">
                                        <h5>متابعه شهريه</h5>
                                        <p class="">
                                            نضمن لعملائنا كافة برامجنا وحتى سنوات من تنفيذها, فنحن نملك الخبرة لتقديم ضمان على كافة خدماتنا لسنوات
                                        </p>                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="box-icon">
                                    <div class="icon">
                                        <div class="fa fa-briefcase"></div>
                                    </div>
                                    <div class="box-icon-body">
                                        <h5>ازاله تامه للحشرات</h5>
                                        <p class="">
                                            نقضي على الحشرات نهائيا بخبرة لأكثر من 10 سنوات فى مجال مكافحه الحشرات وبأحدث الطرق العلميه والاساليب التكنولوجية 
                                        </p>                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="box-icon">
                                    <div class="icon">
                                        <div class="fa fa-money"></div>
                                    </div>
                                    <div class="box-icon-body">
                                        <h5>اسعار مقبوله للجميع</h5>
                                        <p class="">
                                            تقدم الشركة عروض بشكل مستمر لتصل خدماتنا للجميع بأفضل الأسعار. 
                                        </p>                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>

        </div>
    </div>
</div>

<!-- PEST CONTROL -->
<div class="section who bgc">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="section-title">
                    <h3 class="lead">من نحن</h3>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-6 col-md-6">
                <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                    <ul id="myTabs" class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="about.html#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="fa fa-bookmark"></span></a></li>
                        <li class=""><a href="about.html#tab2" aria-controls="tab2" role="tab" data-toggle="tab"><span class="fa fa-flag"></span></a></li>
                        <li class=""><a href="about.html#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><span class="fa fa-lightbulb-o"></span></a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab1">
                            <div class="body-tab">
                                <h5>اهداف الشركة</h5>
                                <p class="">
                                     تعتبر شركة صلاح الدين لتنظيف المباني و ابادة ومكافحة الحشرات من اوائل الشركات التى تعتمد على استخدام التقنيات الحديثة فى عمليات التنظيف للمباني والفيلات والسجاد ومكافحة الحشرات ، و تتميز الشركه بأن لدينا افضل العمالة المدربة ومهندسين متخصصون فى مجال تنظيف المباني و مكافحة الحشرات لذلك تقوم الشركة بمتابعه دائمة ومستمرة للوصول لأفضل النتائج . 
                                </p>                               </div>
                        </div>
                        
                    </div>
                </div>

                <div class="download-brochure">
                    <a href="about.html#" title="" class="btn btn-brochure">
                        <span class="fa fa-file-pdf-o doc"></span>تحميل البيانات
                        <span class="caret-brochure"></span>
                    </a>
                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <div id="about-caro" class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="{{ $theme }}images/about-2.jpg" alt="" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="{{ $theme }}images/about-2.jpg" alt="" class="img-fluid" />
                    </div>
                    <div class="item">
                        <img src="{{ $theme }}images/about-2.jpg" alt="" class="img-fluid" />
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>

<!-- STATS  -->
<div class="section stats bg1">
    <div class="container">

        <div class="row">

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-users"></div>
                    </div>
                    <h2 class="counter">20+</h2>
                    <p>موظف</p>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-building-o"></div>
                    </div>
                    <h2 class="counter">50+</h2>
                    <p>عميل</p>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-heart-o"></div>
                    </div>
                    <h2 class="counter">2000+</h2>
                    <p>تابع</p>
                </div>
            </div>

            <div class="col-sm-4 col-md-3">
                <div class="box-stat">
                    <div class="icon">
                        <div class="fa fa-trophy"></div>
                    </div>
                    <h2 class="counter">24+</h2>
                    <p>عميل</p>
                </div>
            </div>

        </div>
    </div>
</div>

@stop


@section("footer")
<!--JAVA SCRIPT-->

<!--JQUERY SCROPT-->
<script src="{{ $theme }}js/jquery.min.js"></script>

<!--BOOTSTRAP SCRIPT-->
<script src="{{ $theme }}bootstrap/js/bootstrap.min.js"></script>

<!--SLIDER SCRIPT-->
<script src="{{ $theme }}owl/js/owl.carousel.js"></script>
<script src="{{ $theme }}owl/js/jquery.superslides.js"></script>

<!--CUSTOM JS-->
<script src="{{ $theme }}js/main.js"></script>

@stop