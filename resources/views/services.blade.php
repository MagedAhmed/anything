@extends("theme.layout")
@section("header")
    <!--BOOTSTRAP CSS STYLE-->
    <link href="{{ $theme }}bootstrap/css/bootstrap.min.rtl.css" rel="stylesheet" type="text/css">

    <!--Font Awesome css-->
    <link href="{{ $theme }}font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--OWL SLIDER CSS STYLE-->
    <link href="{{ $theme }}owl/css/owl.carousel.min.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}owl/css/owl.theme.default.min.rtl.css" rel="stylesheet" type="text/css">

    <!--CUSTOME CSS-->
    <link href="{{ $theme }}css/main.rtl.css" rel="stylesheet" type="text/css">

    <link href="{{ $theme }}css/index.rtl.css" rel="stylesheet" type="text/css">
    <link href="{{ $theme }}css/services.rtl.css" rel="stylesheet" type="text/css">

    <script src="{{ $theme }}js/modernizr.min.js"></script>

@stop


@section("content")
<!-- BANNER -->
<div class="section subbanner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="caption">خدماتنا</div>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">الرئيسيه</a></li>
                    <li class="active">خدماتنا</li>
                </ol>

            </div>
        </div>
    </div>
</div>

<!-- WHY  -->
<div class="section service">
    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-12">
                <div class="box-service">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    تنظيف المباني والفيلات
                                </h5>
                                <p class="">
                                    الشركة بها العمالة المدربة والأجهزة المناسبة لتنظيف المباني والفيلات من الداخل والخارج والحصول علي افضل النتائج في أسرع وقت ممكن
                                </p>
                                <p class="">
                                    تقدم الشركة عروض بشكل مستكمل وتقدم افضل خدمة ممكنة في اسرع وقت لراحة العميل وإرضائه .
                                </p>
                                <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 text-xs-right">
                            <div class="box">
                                <img src="{{ $theme }}/images/tanzeef.jpg" alt="تنظيف المباني" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                   القضاء على الصراصير
                                </h5>
                                <p class="">
                                    حشرة من رتبة مستقيمة الأجنحة صغير له ست أرجل، أجنحة شفافة بنية اللون، جسمه مفلطح بيضي يضرب لونه للسمرة، و له قرنا الاستشعار طويلان والعينان كبيرتان وللصرصور رائحة كريهة من إفراز غدي
                                </p>
                                
                                <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/pest-1.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    تنظيف السجاد
                                </h5>
                                <p class="">
                                    يعدّ السجاد من القطع المهمة في المنزل، ولا يمكن اعتبارها من كماليات المنزل، بل هي ضرورية لجعل المنزل أجمل، وإظهار الأثاث بصورة أكثر أناقة، كما تساعد على دفء المنزل في فصل الشتاء، ولكن غسل السجاد يتطلب الكثير من الوقت والجهد

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/segad.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                   مكافحه البق
                                </h5>
                                <p class="">
                                    بق الفراش حشرة صغيره (غير مجنحة وطولها (4 - 7 مليمتر) ولونها يميل إلى البني الداكن وبيضاوية الشكل) تمص الدماء أثناء الليل وتسبب الحكة الشديدة والحساسية والإرهاق بسبب عدم النوم، تتغذى على الدم خصوصا أثناء نوم الإنسان ليلا أو نهاراً في الغرف المظلمة
                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/ba2.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>


                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    مكافحه الثعابين والأفاعي
                                </h5>
                                <p class="">
                                    الثعبان أو الأفعى أو الحية هو حيوان زاحف من ذوات الدم البارد يتبع رتيبة Serpentes من رتبةالحرشفيات له جسم متطاول، مغطى بحراشف، ولا توجد له أطراف، أو أذنين خارجيتين، وجفون ولكن ثمة حواف في جسمه، يعتقد أنها كانت تمثل أطرافه التي تلاشت

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/t3abeen.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    مكافحه الرمة
                                </h5>
                                <p class="">
                                    النمل الأبيض حشرة كانسة كما يتسم بحياته السرية, ويتغذى أساسا على السيليولوز, ويعيش في مستعمرات تتنوع فيها الطوائف التي تختلف في بنيتها ووظيفة كل منها
                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/elrama.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    مكافحه الحشرات الطائره
                                </h5>
                                <p class="">
                                    بإستخدام أفضل المبيدات الحشرية والمستخدمة عالميا فى عمليات الرش في اماكن تواجد
                            الحشرات بإستخدام أحدث الأدوات والمعدات التكنولوجية والأمنه عند الإستخدام، والتي لا
                            تؤثر على صحة الإنسان والصحه العامه.

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/pest-6.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    القضاء على النمل
                                </h5>
                                <p class="">
                                    يعد النمل من الحشرات التى تؤثر سلبيا على حياة الانسان حيث يتميز بسرعة التكاثر والانتشار بشكل كبير جدا ليغزو المنازل والشركات والمطاعم والمخازن للبحث عن المأوى والغذاء مما يؤدى الى فساد العديد من المواد الغذائية والتى تحتوى على السكريات مما يؤدى الى تلوث المواد الغذائية وانتشار البكتريا والجراثيم عليها مما يشكل خطوة كبرى على الانسان

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/pest-7.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    مكافحه الفئران
                                </h5>
                                <p class="">
                                    تعد الفئران من اكثر القوارض انتشارا, حيث تتواجد فى غالبية اسطح المبانى والشركات والمخازن والمطاعم والمزارع, وتتسبب الفئران فى العديد من المشاكل إن تواجدت فى منزلك حيث تسبب العديد من الاضرار بالملابس والاسلاك والاساس المنزلي, ايضا يوجد العديد من الامراض التى قد تتسبب بها.

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/pest-8.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    التخلص من النحل
                                </h5>
                                <p class="">
                                    لسعة النحل هي طريقة دفاعية تقوم بها نحلة أو مجموعة من النحل للدفاع عن نفسها أو عن عشها من الآفات الشرسة كالحشرات أو الحيوانات التي تأكلها أو تأكل عسلها أو حتى الإنسان تقوم بالهجوم عليه لأسباب منها أخذ العسل من عشها[1] النحل يموت عندما يلسع اي كائن و ذلك يرجع إلى انفصال جزء من امعاء النحلة.

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/na7l.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    التخلص من العقارب
                                </h5>
                                <p class="">
                                    يعيش في المناطق الحارة والجافّة، مختبئا في الجحور والشقوق ،وتحت الحجارة والصخور، التماسا للرطوبة ،وتجنبا لحرارة الشمس.يتواجد من العقارب في العالم حوالي 2000 نوع.معظم العقارب سامة إذ تتواجد الغدة السامة في نهاية الذيل.

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/scorpion.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-service">
                    <div class="row">

                        <div class="col-sm-6 col-md-6 text-xs-left">
                            <div class="description">
                                <h5 class="blok-title">
                                    تنظيف الموكيت
                                </h5>
                                <p class="">
                                    في بعض المنازل يتم استخدام الموكيت كحل عملي في المساحات الكبيرة و غرف اﻷطفال ، ولكن تأتي مهمة تنظيفه بشكل دوري أمرًا غاية في اﻷهمية حتي لا يتسبب تراكم اﻷتربة في التأثير على صحة اﻷسرة .

                                </p>
                                    
                                    <a href="{{url('/contact')}}" title="" class="btn btn-default btn-view-all">اتصل بنا لمعرفة المزيد</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="box">
                                <img src="{{ $theme }}/images/moket.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>





            </div>

        </div>
    </div>
</div>


@stop


@section("footer")
<!--JAVA SCRIPT-->

<!--JQUERY SCROPT-->
<script src="{{ $theme }}js/jquery.min.js"></script>

<!--BOOTSTRAP SCRIPT-->
<script src="{{ $theme }}bootstrap/js/bootstrap.min.js"></script>

<!--SLIDER SCRIPT-->
<script src="{{ $theme }}owl/js/owl.carousel.js"></script>
<script src="{{ $theme }}owl/js/jquery.superslides.js"></script>

<!--CUSTOM JS-->
<script src="{{ $theme }}js/main.js"></script>
@stop

