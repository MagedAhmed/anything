<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
        	'name' => 'maged',
        	'email' => 'maged@maged.com',
        	'role' => 'admin',
        	'password' => bcrypt(123456)
        	]);
    }
}
