$(document).ready(function() {

    "use strict";
    /* =================================
     NAVBAR
     =================================== */
    jQuery(window).scroll(function () {
        var top = jQuery(document).scrollTop();
        var batas = jQuery(window).height();

        if (top > batas) {
            jQuery('.navbar-main').addClass('stiky');
        } else {
            jQuery('.navbar-main').removeClass('stiky');
        }
    });

    /* =================================
     BANNER ROTATOR IMAGE
     =================================== */
    $('#slides').superslides({
        animation: "fade",
        play: 5000,
        slide_speed: 800,
        pagination: false,
        hashchange: false,
        scrollable: true
    });

    /* =================================
     OWL
     =================================== */

    var about = $("#about-caro");
    about.owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: true
    });

    var owl = $("#owl-testimony");
    owl.owlCarousel({
        autoplay: 5000,
        stopOnHover: true,
        margin: 30,
        items: 2,
        nav: true,
        navText: ["<span class='fa fa-chevron-left'></span>", "<span class='fa fa-chevron-right'></span>"],
        dots: false,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    });

});
